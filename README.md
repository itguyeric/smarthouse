# IT Guys House

Help design the open-source, self-hosted automated house of dreams!

## Description

In 18 months, I hope to be able to build my own home. I want it to be as self-sufficient as possible, green as can be, as well as automated and decked out with self-hosted, open source solutions! (Bonus points if I can test or implement the solution in a 2 bedroom apartment ahead of time!)

In true open source spirit, I am opening this endeavour up to the community. I welcome your feedback and ideas.